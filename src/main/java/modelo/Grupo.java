package modelo;
// Generated 15 dic 2022 16:34:03 by Hibernate Tools 4.3.6.Final


import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

/**
 * Grupo generated by hbm2java
 */
@Entity
@Table(name = "grupo", catalog = "emphb")
public class Grupo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "descripcion", nullable = false, length = 15)
	private String descripcion;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "grupo")
	private Set<Articulo> articulos = new HashSet<Articulo>(0);

	public Grupo() {
	}

	public Grupo(String descripcion) {
		this.descripcion = descripcion;
	}

	public Grupo(String descripcion, Set<Articulo> articulos) {
		this.descripcion = descripcion;
		this.articulos = articulos;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set<Articulo> getArticulos() {
		return this.articulos;
	}

	public void setArticulos(Set<Articulo> articulos) {
		this.articulos = articulos;
	}
	
	@Override
	public String toString() {
		return "Grupo [id=" + id + ", descripcion=" + descripcion + ", total articulos: " + articulos.size() + "]";
	}

}
